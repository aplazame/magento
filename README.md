[![](docs/config.png)](https://aplazame.com) 

**Compatible with**

Magento CE 1.4.0.1+

Install
-------

**To install using [modgit](https://github.com/jreinke/modgit)**

```
cd MAGENTO_ROOT
modgit init
modgit -i extension/:. add Magento_Aplazame https://bitbucket.org/aplazame/magento
```
to update:
```
modgit update Magento_Aplazame
```

**To install using [modman](https://github.com/colinmollenhour/modman)**

```
cd MAGENTO_ROOT
modman clone https://bitbucket.org/aplazame/magento
```
to update:
```
modman update Magento_Aplazame
```

**To install using Aplazame's deploy script**

1. Download the [Makefile](https://raw.githubusercontent.com/Aplazame/Magento_Aplazame/master/util/Makefile) (requires git, wget)
2. Copy to MAGENTO_ROOT
3. To install, run `make install`
4. To update, run `make update`

Configure
---------

**Payment Method**

1. Log in to your Magento Admin portal.
2. Visit System > Cache Management. Then, click _Flush Magento Cache_
2. Visit System > Configuration > Payment Methods > Aplazame
3. Set the API URL. On your live site, use ```https://api.aplazame.com```.
4. Provide your 2 api tokens (merchant API key, secret key
5. Adjust the order total minimum and maximum options to control when Aplazame is
   shown to your customers.
 
	![](docs/config.png)
