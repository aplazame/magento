<?php

class Aplazame_Aplazame_Model_Api_Versions
{
    public function toOptionArray()
    {
        return array(
            array('value'=>'v1', 'label'=>'v1'),
            array('value'=>'v2', 'label'=>'v2')
        );
    }

}